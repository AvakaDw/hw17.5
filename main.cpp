#include <iostream>
#include <math.h>

class Vector {

private:
	double x, y, z;

public:
	void ShowXyz() {
		std::cout << "XYZ coordinates: " << x << ' ' << y << ' ' << z;
	}

	Vector() : x{ 0.0 }, y{ 0.0 }, z{ 0.0 }
	{}

	void SetVector() {
		std::cout << "Enter x: ";
		std::cin >> x;
		std::cout << "Enter y: ";
		std::cin >> y;
		std::cout << "Enter z: ";
		std::cin >> z;
	}

	double VectorModule() {
		double VM;
		VM = sqrt((pow(x, 2) + pow(y, 2) + pow(z, 2)));
		return VM;
	}
};

int main() {
	Vector h;
	h.SetVector();
	h.ShowXyz();
	std::cout << '\n' << "Vector Module: " << h.VectorModule();
}